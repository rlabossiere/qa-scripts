$Servers = "env16-casa.ocius.net", "env17-casa.ocius.net", "env18-casa.ocius.net", "env19-casa.ocius.net", "env20-casa.ocius.net"
$StartTime = Get-Date

Write-Output "This script will restart AutoQA Envs.`n"
Write-host "Would you like to restart AutoQA Envs?"
    $Readhost = Read-Host "( Y / N )" 
    Switch ($ReadHost) 
     { 
       Y {
            Workflow Workflow-Restart
            {
                param (
                [DateTime]$StartTime,
                [string[]]$Servers
                )

                foreach -parallel($server in $Servers) 
                {
	                Restart-Computer -PSComputerName $server -For WinRM -Force -Wait
	                $EndTime = Get-Date
	                $minutes = (New-TimeSpan $StartTime $EndTime).Minutes
	                $seconds = (New-TimeSpan $StartTime $EndTime).Seconds
	                Write-Output "Time Taken for $server to come online: $minutes minutes : $seconds seconds"
                    pause
                }
            }
       } 
       N {exit} 
       Default {exit} 
     } 

Workflow-Restart $StartTime $Servers