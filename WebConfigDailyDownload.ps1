# Self-elevating to run as Administrator. Required for modifying Web.config on the C:
if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) { Start-Process powershell.exe "-NoProfile -ExecutionPolicy Bypass -File `"$PSCommandPath`"" -Verb RunAs; exit }

Write-Output "This script will modify firewall settings and various Web.config files with test-specific values.`n"

# Load all of our Web.config XML files into objects
$pcdConfigFile="C:\inetpub\PCD\Web.config"
$plapiConfigFile="C:\inetpub\plapi.ocius.net\Web.config"
$payleaseConfigFile="C:\inetpub\PayLeaseBillingApplication\Web.config"

# Will be used for ShareFileFTP
$ftpname = Read-Host 'What is your ShareFileFTP username?'
$ftppass = Read-Host 'What is your ShareFileFTP password?' -AsSecureString
$envNum = Read-Host 'Which ENV number are you looking to modify?'

# Key-Value pairs to search for and replace in the Web.config files
$modifiedSettings = @{
    "Mandrill" = "smtp.mandrillapp.com"
    "MandrillFrom"  = "noreply@paylease.com"
	"MandrillReplyTo" = "noreply@paylease.com"
	"MandrillPort" = "587"
	"MandrillUsername" = "mandrill@paylease.com"
	"MandrillPwd" = "65-KuAMy5GIY30l8x2T3jg"
	"EcovaImagesDump" = "D:\UPLUS_AIQ\TempDownload\" # Beginning of Ecova and ShareFileFTP
	"EcovaImagesException" = "D:\UPLUS_AIQ\Exceptions"
	"EcovaImagesDestination" = "D:\UPLUS_AIQ\ExpenseReductionImages"
	"EcovaFTPHost" = "ocius.sharefileftp.com"
	"EcovaImagesServer" = "\\web3\D$"
	"EcovaImagesRemoteLoc" = "/EcovaQA/Images$envNum"
	"EcovaBillsRemoteLoc" = "/EcovaQA/Ecova$envNum"
	"EcovaBillsDump" = "D:\UPLUS_AIQ\AvistaDownloads"
	"EcovaRefundLocation" = "D:\UPLUS_AIQ\Refunds"
	"EcovaRefundRemoteLoc" = "/EcovaQA/Ecova$envNum/Refund Check Report $envNum"
	"EcovaPortNumber" = "21"
	"EcovaSummaryRemoteLoc" = "/EcovaQA/Ecova$envNum/Summary$envNum"
	"EcovaDataDump" = "D:\UPLUS_AIQ\"
	"FtpUserName" = "ocius/$ftpname@paylease.com"
	"FtpPassword" = [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR($ftppass)) # This is the plain text masked password we read as input
	"FtpServiceName" = "ShareFile"
	"AWSBucket" = "utilitydataservicebillimages-qa" # QA AWS for Engie Bill Images
	"PayableAchFileLocation" = "D:\UPLUS_AIQ\Finance" # Beginning of ACH File Generation
	"WintrustFileNameFormat" = "RY0HA380.ocius_ccd_ACH_{date}.txt"
	"WintrustSFTPHost" = ""
	"WintrustSFTPFolder" = ""
	"WintrustUsername" = ""
	"WintrustKeyFileLocation" = ""
}

# Allow the firewall to send Emails over SMTP.
$firewallRule = "Block SMTP Port - Octopus"
Write-Output "Modifying Firewall Rules...`n"
try {
	Set-NetFirewallRule -DisplayName $firewallRule -Enabled False -ErrorAction stop
} catch [Microsoft.PowerShell.Cmdletization.Cim.CimJobException] {
	Write-Output "Error occurred when trying to disable $firewallRule. Does rule exist?`n"
}

$WebConfigFiles = @($PCDConfigFile,$plapiConfigFile,$payleaseConfigFile)

foreach ($configFile in $WebConfigFiles) {
	try {
		[xml]$configXml = Get-Content $configFile -ErrorAction stop
		
			# Rename the old Web.config file to preserve it as a backup.
			Write-Output "Saving backup as $configFile.bak...`n"
			try {
				Rename-Item -Path $configFile -NewName "$configFile.bak" -ErrorAction stop
			} catch [System.Exception] {
				Write-Output "Error occurred when trying to take a backup of $configFile. File not found or backup may already exist.`n"
			}
		
		# This is ugly.
		# If our map of modified settings contains the current appSettings key, replace its value with ours.
		Write-Output "Modifying Web.config...`n"
		$appSettings = $configXml.configuration.appSettings.add
		foreach ($setting in $appSettings) {
			if ($modifiedSettings[$($setting.key)] -ne $null) {
				Write-Output "Changing $($setting.key) to $($modifiedSettings[$($setting.key)])"
				$setting.value=$modifiedSettings[$($setting.key)]
			}
		}
		
		# Save our modifed XML object back to file.
		if ($configXml -ne $null) {
			$configXml.Save($configFile)
		} else {
			Write-Output "Errors occurred. Did not modify $configFile."
		}
	} catch [System.Management.Automation.ItemNotFoundException]{
		Write-Output "Error occurred when trying to load XML, does $configFile exist?`n"
	}
}

Write-Output "`nComplete!"
pause