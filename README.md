# QA Scripts

Script Name | Function
------------ | -------------
WebConfigDailyDownload | Script to edit firewall settings & change Web.config values to allow the sending of test emails and daily downloads
RestartAutoQAEnvs | Script to restart AutoQA Envs

### Changelog
* 19-Sept-2018 - Initial commit
* 2-Oct-2018 - Adding some additional variables & configuration for UEM/ACH, not implemented at this time
* 3-Oct-2018 - Initial refactor to support PCD UEM/ACH
* 10-Oct-2018 - Fix for FtpUsername, and now using user input for the ENV numbers
* 30-Oct-2018 - Renamed the Web.config Daily Download modification script to better reflect what it does
* 01-Mar-2019 - Added script to restart AutoQA Envs. Could be enhanced by checking for Octopus connection